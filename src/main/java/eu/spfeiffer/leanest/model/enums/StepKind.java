package eu.spfeiffer.leanest.model.enums;

public enum StepKind {
    GIT_CHECKOUT, CMD, CLEANUP;
}
