package eu.spfeiffer.leanest.model.enums;

public enum OutputChannel {
    OUT, ERR, INT;
}
