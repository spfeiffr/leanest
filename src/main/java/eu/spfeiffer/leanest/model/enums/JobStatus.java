package eu.spfeiffer.leanest.model.enums;

public enum JobStatus {

    NOT_YET_STARTED("fa-pause-circle grey"), RUNNING("fa-cog blue animated"), FAILED("fa-heart-broken red"), SUCCEEDED(
	    "fa-check-circle green"), ABORTED("fa-ban red"), SKIPPED("fa-forward grey");

    private final String icon;

    JobStatus(String icon) {
	this.icon = icon;
    }

    public String toString() {
	return icon;
    }
}
