package eu.spfeiffer.leanest.model.enums;

public enum ExecutionRule {
    ON_SUCCESS, ON_FAILURE, ALWAYS;
}
