package eu.spfeiffer.leanest.model.ui;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.ocpsoft.prettytime.PrettyTime;

import eu.spfeiffer.leanest.model.entities.Job;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class JobUiModel extends Job {
    private String startedAgo;
    private String formattedStartedAt;
    private String formattedFinishedAt;
    private String runningFor;

    public JobUiModel(Job job) {
	this.setId(job.getId());

	this.setVersion(job.getVersion());
	this.setEnvironment(job.getEnvironment());
	this.setWorkflow(job.getWorkflow());
	this.setUser(job.getUser());

	this.setStatus(job.getStatus());

	this.setStartedAt(job.getStartedAt());
	this.setFormattedStartedAt(Optional.ofNullable(job.getStartedAt()).map(i -> i.truncatedTo(ChronoUnit.SECONDS))
		.map(i -> DateTimeFormatter.ISO_INSTANT.format(i)).orElse("now"));
	this.setFinishedAt(job.getFinishedAt());
	this.setFormattedFinishedAt(Optional.ofNullable(job.getFinishedAt()).map(i -> i.truncatedTo(ChronoUnit.SECONDS))
		.map(i -> DateTimeFormatter.ISO_INSTANT.format(i)).orElse("now"));

	PrettyTime p = new PrettyTime();
	this.setStartedAgo("");
	this.setRunningFor("");
	if (Objects.nonNull(job.getStartedAt())) {
	    this.setStartedAgo(p.format(Date.from(job.getStartedAt())));

	    Duration running = Duration.between(job.getStartedAt(),
		    Optional.ofNullable(job.getFinishedAt()).orElseGet(Instant::now));
	    if (running.getSeconds() < 60) {
		this.runningFor = String.format("%d sec", running.getSeconds());
	    } else {
		this.runningFor = String.format("%d min", Math.floorDiv(running.getSeconds(), 60L));
	    }
	}
    }
}
