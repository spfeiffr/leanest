package eu.spfeiffer.leanest.model;

import lombok.Data;

@Data
public class ExecutionRequest {
    private String version;
    private String environment;
    private String workflow;
}
