package eu.spfeiffer.leanest.model.entities;

import java.time.Instant;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import eu.spfeiffer.leanest.model.enums.ExecutionRule;
import eu.spfeiffer.leanest.model.enums.JobStatus;
import eu.spfeiffer.leanest.model.enums.StepKind;
import lombok.Data;

@Data
@Embeddable
public class JobStep {
    private Integer step;

    @Enumerated(EnumType.STRING)
    private StepKind kind;

    @Enumerated(EnumType.STRING)
    private ExecutionRule executeOn;

    private String playbook;
    private Instant startedAt;
    private Instant finishedAt;

    @Enumerated(EnumType.STRING)
    private JobStatus status;

    private int exitCode;
}
