package eu.spfeiffer.leanest.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class StepLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private long job;
    private int step;

    private String log;
    private long linecount;

    public StepLog(long job, int step) {
	this();
	this.job = job;
	this.step = step;
	this.log = new String();
	this.linecount = 0;
    }

}
