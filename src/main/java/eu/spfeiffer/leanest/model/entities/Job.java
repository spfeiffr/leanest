package eu.spfeiffer.leanest.model.entities;

import java.time.Instant;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderBy;

import eu.spfeiffer.leanest.model.enums.JobStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String version;
    private String environment;
    private String workflow;
    private String user;

    @Enumerated(EnumType.STRING)
    private JobStatus status;

    private Instant startedAt;
    private Instant finishedAt;

    @ElementCollection
    @OrderBy("step")
    private List<JobStep> steps;
}
