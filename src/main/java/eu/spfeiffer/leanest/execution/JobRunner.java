package eu.spfeiffer.leanest.execution;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.ShutdownHookProcessDestroyer;
import org.springframework.core.task.TaskExecutor;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;

import eu.spfeiffer.leanest.config.LeanestConfig;
import eu.spfeiffer.leanest.model.entities.Job;
import eu.spfeiffer.leanest.model.entities.JobStep;
import eu.spfeiffer.leanest.model.entities.StepLog;
import eu.spfeiffer.leanest.model.enums.ExecutionRule;
import eu.spfeiffer.leanest.model.enums.JobStatus;
import eu.spfeiffer.leanest.model.enums.StepKind;
import eu.spfeiffer.leanest.repository.StepLogRepository;
import eu.spfeiffer.leanest.service.JobExecutionService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JobRunner implements Runnable {
    private final Job job;
    private final JobExecutionService jobExecService;
    private final StepLogRepository logRepository;
    private final TaskExecutor taskExecutor;
    private final LeanestConfig config;

    @Override
    public void run() {
	JobStatus lastStepStatus = JobStatus.SUCCEEDED;
	Path workdir = Paths.get(config.getWorkDir(), job.getId().toString());
	try {
	    FileSystemUtils.deleteRecursively(workdir);
	} catch (IOException e2) {
	    e2.printStackTrace();
	}

	job.setStatus(JobStatus.RUNNING);
	for (JobStep step : job.getSteps()) {
	    // Skip steps that should only run on success when last step failed
	    if (lastStepStatus != JobStatus.SUCCEEDED && step.getExecuteOn() == ExecutionRule.ON_SUCCESS) {
		step.setStatus(JobStatus.SKIPPED);
		jobExecService.updateJob(job);
		continue;
	    }

	    // Skip steps that should only run on failure when last step
	    // succeeded
	    if (lastStepStatus == JobStatus.SUCCEEDED && step.getExecuteOn() == ExecutionRule.ON_FAILURE) {
		step.setStatus(JobStatus.SKIPPED);
		jobExecService.updateJob(job);
		continue;
	    }

	    step.setStatus(JobStatus.RUNNING);
	    step.setStartedAt(Instant.now());
	    jobExecService.updateJob(job);

	    CommandLine cmdLine = null;
	    String input = null;
	    Map<String, String> env = new HashMap<>();
	    switch (step.getKind()) {
	    case GIT_CHECKOUT:
		String branchname = config.getVersions().get(job.getVersion()).getBranch();

		cmdLine = new CommandLine("git");
		cmdLine.addArgument("clone");
		cmdLine.addArgument("-v");
		cmdLine.addArgument("--depth=1");
		cmdLine.addArgument("--single-branch");
		cmdLine.addArgument("-b");
		cmdLine.addArgument(branchname);
		cmdLine.addArgument(config.getGitRemote());
		cmdLine.addArgument(workdir.toString());

		break;
	    case CLEANUP:
		StepLog cleanupOutput = new StepLog(job.getId(), step.getStep());
		try {
		    FileSystemUtils.deleteRecursively(workdir);
		    step.setStatus(JobStatus.SUCCEEDED);
		    cleanupOutput.setLog(String.format("Successfully deleted %s", workdir));

		} catch (IOException e1) {
		    step.setStatus(JobStatus.FAILED);
		    cleanupOutput.setLog(String.format("Failed to delete %s: %s %s", workdir,
			    e1.getClass().getSimpleName(), e1.getMessage()));
		}
		logRepository.save(cleanupOutput);
		break;
	    case CMD:
		cmdLine = new CommandLine("sshpass");
		cmdLine.addArgument("-e");
		cmdLine.addArgument("ansible-playbook");
		cmdLine.addArgument(step.getPlaybook());
		cmdLine.addArguments(
			new String[] { "-i", config.getEnvironments().get(job.getEnvironment()).getInventory() });
		cmdLine.addArguments(new String[] { "-K", "-v", "--ask-sudo-pass" });
		env.put("ANSIBLE_HOST_KEY_CHECKING", "false");
		env.put("SSHPASS", config.getEnvironments().get(job.getEnvironment()).getSudoPassword());
		break;
	    }

	    if (Objects.nonNull(cmdLine)) {
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

		ExecuteWatchdog watchdog = new ExecuteWatchdog(7200 * 1000);
		Executor executor = new DefaultExecutor();
		executor.setWatchdog(watchdog);
		executor.setProcessDestroyer(new ShutdownHookProcessDestroyer());
		if (step.getKind() == StepKind.CMD) {
		    executor.setWorkingDirectory(workdir.toFile());
		}
		JobStreamHandler streamHandler = new JobStreamHandler(job.getId(), step.getStep(), taskExecutor, input,
			logRepository);
		executor.setStreamHandler(streamHandler);

		try {
		    executor.execute(cmdLine, env, resultHandler);
		    while (!resultHandler.hasResult()) {
			if (jobExecService.isJobAbortionRequested(job)) {
			    watchdog.destroyProcess();
			}
			resultHandler.waitFor(3000L);
		    }
		} catch (ExecuteException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (InterruptedException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

		step.setExitCode(resultHandler.getExitValue());
		if (step.getExitCode() == 0) {
		    step.setStatus(JobStatus.SUCCEEDED);
		    if (step.getKind() == StepKind.GIT_CHECKOUT) {
			cmdLine = new CommandLine("git");
			cmdLine.addArgument("rev-parse");
			cmdLine.addArgument("--short");
			cmdLine.addArgument("HEAD");
			executor = new DefaultExecutor();
			executor.setWatchdog(watchdog);
			executor.setProcessDestroyer(new ShutdownHookProcessDestroyer());
			executor.setWorkingDirectory(workdir.toFile());

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			PumpStreamHandler streamToStringHandler = new PumpStreamHandler(outputStream);
			executor.setStreamHandler(streamToStringHandler);

			try {
			    executor.execute(cmdLine);
			} catch (ExecuteException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			} catch (IOException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}

			String rev = StringUtils.trimTrailingWhitespace(outputStream.toString());
			step.setPlaybook(String.format("%s @%s", step.getPlaybook(), rev));
			job.setVersion(String.format("%s @%s", job.getVersion(), rev));
		    }
		} else {
		    if (jobExecService.isJobAbortionRequested(job)) {
			step.setStatus(JobStatus.ABORTED);
		    } else {
			step.setStatus(JobStatus.FAILED);
		    }
		}
	    }

	    // Only update lastStepStatus if we are not already failed / aborted
	    if (lastStepStatus == JobStatus.SUCCEEDED) {
		lastStepStatus = step.getStatus();
	    }
	    step.setFinishedAt(Instant.now());
	    jobExecService.updateJob(job);
	}

	job.setFinishedAt(Instant.now());
	job.setStatus(lastStepStatus);
	jobExecService.updateJob(job);
    }

}
