package eu.spfeiffer.leanest.execution;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

import org.apache.commons.exec.ExecuteStreamHandler;
import org.springframework.core.task.TaskExecutor;

import eu.spfeiffer.leanest.model.entities.StepLog;
import eu.spfeiffer.leanest.model.enums.OutputChannel;
import eu.spfeiffer.leanest.repository.StepLogRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JobStreamHandler implements ExecuteStreamHandler, Runnable {

    private InputStream os = null;
    private InputStream errs = null;
    private OutputStream is = null;

    private boolean running = false;
    private long line = 0;

    private final long job;
    private final int step;
    private final TaskExecutor taskExecutor;
    private final String input;
    private final StepLogRepository repository;

    private byte[] buf = null;
    private Instant startInstant;
    private StepLog log;

    @Override
    public void setProcessInputStream(OutputStream os) throws IOException {
	is = os;
    }

    @Override
    public void setProcessErrorStream(InputStream is) throws IOException {
	errs = is;
    }

    @Override
    public void setProcessOutputStream(InputStream is) throws IOException {
	os = is;
    }

    @Override
    public void start() throws IOException {
	running = true;
	line = 1;
	buf = new byte[1024];
	startInstant = Instant.now();
	log = new StepLog(job, step);
	taskExecutor.execute(this);
    }

    @Override
    public void stop() throws IOException {
	running = false;
	buf = null;
    }

    @Override
    public void run() {
	try {
	    if (Objects.nonNull(input)) {
		is.write(input.getBytes());
	    }
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	String errbuf = "";
	String outbuf = "";

	while (running) {
	    slurpStream(errs, OutputChannel.ERR, errbuf);
	    slurpStream(os, OutputChannel.OUT, outbuf);
	}

	slurpStream(errs, OutputChannel.ERR, errbuf);
	slurpStream(os, OutputChannel.OUT, outbuf);
    }

    private void slurpStream(InputStream stream, OutputChannel channel, String linebuffer) {
	try {
	    while (stream.available() > 0) {
		int bytes = stream.available();
		if (bytes > 0) {
		    if (bytes > buf.length) {
			bytes = buf.length;
		    }
		    stream.read(buf, 0, bytes);
		    String str = linebuffer + new String(buf, 0, bytes, StandardCharsets.UTF_8);
		    str = str.replaceAll("\\\\\\\\n", "\n");
		    str = str.replaceAll("\\\\\\\\t", "    ");
		    String[] lines = str.split("\\R", -1);
		    int len = lines.length - 1;
		    linebuffer = lines[len];
		    StringBuilder sb = null;
		    for (int i = 0; i < len; i++) {
			if (Objects.isNull(sb)) {
			    sb = new StringBuilder(log.getLog());
			}
			String s = lines[i];
			Duration duration = Duration.between(startInstant, Instant.now());
			String newLine = String.format("%3d:%02d.%03d | %5d | %3s | %s\n", duration.getSeconds() / 60,
				duration.getSeconds() % 60, duration.getNano() / 1000000, line, channel, s);
			sb.append(newLine);
			line = line + 1;
		    }

		    if (Objects.nonNull(sb)) {
			log.setLog(sb.toString());
			log.setLinecount(line - 1);
			repository.save(log);
		    }
		}
	    }
	} catch (IOException e) {
	    // Ignore, stream is closed
	}
    }

}
