package eu.spfeiffer.leanest.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import eu.spfeiffer.leanest.config.LeanestConfig;
import eu.spfeiffer.leanest.model.ui.JobUiModel;
import eu.spfeiffer.leanest.repository.JobRepository;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class IndexController {

    private final JobRepository jobRepository;
    private final LeanestConfig leanestConfig;

    @GetMapping("/")
    public ModelAndView index(CsrfToken token, Principal user, Map<String, Object> model) {

	List<String> versions = new ArrayList<String>(leanestConfig.getVersions().keySet());
	Collections.sort(versions);
	List<String> environments = new ArrayList<String>(leanestConfig.getEnvironments().keySet());
	Collections.sort(environments);
	List<String> workflows = new ArrayList<String>(leanestConfig.getWorkflows().keySet());
	Collections.sort(workflows);

	model.put("_csrf", token);
	model.put("user", user.getName());
	model.put("versions", versions);
	model.put("environments", environments);
	model.put("workflows", workflows);

	model.put("jobs", jobRepository.findAll(PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "id"))).stream()
		.map(JobUiModel::new).collect(Collectors.toList()));

	return new ModelAndView("index", model);
    }

    @GetMapping("/update")
    public ModelAndView indexUpdate(CsrfToken token, Principal user, Map<String, Object> model) {

	model.put("jobs", jobRepository.findAll(PageRequest.of(0, 20, Sort.by(Sort.Direction.DESC, "id"))).stream()
		.map(JobUiModel::new).collect(Collectors.toList()));

	return new ModelAndView("index_fragment", model);
    }

    @GetMapping("/account")
    public String redirectToAccountPage(@AuthenticationPrincipal OAuth2AuthenticationToken authToken) {

	if (authToken == null) {
	    return "redirect:/";
	}

	OidcUser user = (OidcUser) authToken.getPrincipal();

	// Provides a back-link to the application
	return "redirect:" + user.getIssuer() + "/account?referrer=" + user.getIdToken().getAuthorizedParty();
    }
}
