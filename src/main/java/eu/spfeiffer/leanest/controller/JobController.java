package eu.spfeiffer.leanest.controller;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import eu.spfeiffer.leanest.model.entities.Job;
import eu.spfeiffer.leanest.model.entities.JobStep;
import eu.spfeiffer.leanest.model.entities.StepLog;
import eu.spfeiffer.leanest.model.enums.JobStatus;
import eu.spfeiffer.leanest.repository.JobRepository;
import eu.spfeiffer.leanest.repository.StepLogRepository;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class JobController {
    private final JobRepository jobRepository;
    private final StepLogRepository logRepository;

    private final String LOGLENGTH_HEADER = "X-Log-Length";
    private final String TITLE_HEADER = "X-Title";
    private final String SUBTITLE_HEADER = "X-Subtitle";
    private final String RELOAD_HEADER = "X-Reload";

    private static final PrettyTime prettyFormatter = new PrettyTime();

    @GetMapping("/job/{jobId}")
    public ModelAndView getJob(CsrfToken token, @PathVariable long jobId, Map<String, Object> model) {

	Optional<Job> optionalJob = jobRepository.findById(jobId);
	model.put("_csrf", token);
	model.put("job", jobId);

	List<Map<String, Object>> steps = new ArrayList<>();

	if (optionalJob.isPresent()) {
	    Job job = optionalJob.get();
	    model.put("title", buildTitle(jobId, job));
	    model.put("subtitle", buildJobSubtitle(job));

	    model.put("steps", job.getSteps().stream().map(step -> JobController.step2Model(job.getId(), step))
		    .collect(Collectors.toList()));
	    if (job.getStatus() == JobStatus.NOT_YET_STARTED || job.getStatus() == JobStatus.RUNNING) {
		model.put("reload", true);
	    }

	} else {
	    model.put("title", String.format("Job %d not found!", jobId));
	    model.put("subtitle", "");
	    model.put("steps", steps);
	}

	return new ModelAndView("job_detail", model);
    }

    @GetMapping("/job/{jobId}/update")
    public ModelAndView getJobFragment(@PathVariable long jobId, Map<String, Object> model) {

	Optional<Job> optionalJob = jobRepository.findById(jobId);

	List<Map<String, Object>> steps = new ArrayList<>();
	if (optionalJob.isPresent()) {
	    Job job = optionalJob.get();
	    model.put("title", buildTitle(jobId, job));
	    model.put("subtitle", buildJobSubtitle(job));
	    model.put("steps", job.getSteps().stream().map(step -> JobController.step2Model(job.getId(), step))
		    .collect(Collectors.toList()));
	    if (job.getStatus() == JobStatus.NOT_YET_STARTED || job.getStatus() == JobStatus.RUNNING) {
		model.put("reload", true);
	    }
	} else {
	    model.put("steps", steps);
	}

	return new ModelAndView("job_detail_fragment", model);
    }

    @GetMapping("/job/{jobId}/step/{step}")
    public ModelAndView getStepLog(@PathVariable long jobId, @PathVariable int step, Map<String, Object> model) {

	Optional<Job> optionalJob = jobRepository.findById(jobId);
	int logLength = 0;
	model.put("job", jobId);
	model.put("step", step);
	model.put("backlink", String.format("/job/%d", jobId));
	if (optionalJob.isPresent()) {
	    Job job = optionalJob.get();
	    Optional<JobStep> optionalStep = job.getSteps().stream().filter(s -> s.getStep() == step).findFirst();

	    model.put("title", buildTitle(jobId, job));
	    if (optionalStep.isPresent()) {
		JobStep jobStep = optionalStep.get();
		Optional<StepLog> log = logRepository.findByJobAndStep(jobId, step);

		model.put("subtitle", buildStepSubtitle(job, jobStep));

		model.put("log", log.map(StepLog::getLog).orElse(""));
		logLength = log.map(StepLog::getLog).map(String::length).orElse(0);
		if (job.getStatus() == JobStatus.NOT_YET_STARTED || job.getStatus() == JobStatus.RUNNING) {
		    model.put("reload", true);
		}

	    } else {
		model.put("subtitle", String.format("Step number %d not found!", step));
	    }

	} else {
	    model.put("title", String.format("Job %d not found!", jobId));
	    model.put("subtitle", "");
	}

	model.put("loglength", Integer.toString(logLength));
	return new ModelAndView("step_detail", model);
    }

    @GetMapping("/job/{jobId}/step/{step}/update")
    public ResponseEntity<String> getStepLogUpdate(@PathVariable long jobId, @PathVariable int step,
	    @RequestParam("start") int startAt) {
	Optional<StepLog> log = logRepository.findByJobAndStep(jobId, step);
	String newLog = log.map(StepLog::getLog).orElse(null);
	HttpHeaders headers = new HttpHeaders();

	if (Objects.isNull(newLog)) {
	    headers.add(RELOAD_HEADER, Boolean.TRUE.toString());
	    return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
	}

	if (startAt >= newLog.length()) {
	    headers.add(RELOAD_HEADER, Boolean.TRUE.toString());
	    return new ResponseEntity<>(headers, HttpStatus.NOT_MODIFIED);
	}

	headers.add(LOGLENGTH_HEADER, Integer.toString(newLog.length()));
	String title = "";
	String subtitle = String.format("Step number %d not found!", step);

	Optional<Job> optionalJob = jobRepository.findById(jobId);
	if (optionalJob.isPresent()) {
	    Job job = optionalJob.get();
	    title = buildTitle(jobId, job);
	    Optional<JobStep> optionalStep = job.getSteps().stream().filter(s -> s.getStep() == step).findFirst();
	    if (optionalStep.isPresent()) {
		JobStep jobStep = optionalStep.get();
		subtitle = buildStepSubtitle(job, jobStep);
		if (job.getStatus() == JobStatus.NOT_YET_STARTED || job.getStatus() == JobStatus.RUNNING) {
		    headers.add(RELOAD_HEADER, Boolean.TRUE.toString());
		}
	    }
	}
	headers.add(TITLE_HEADER, title);
	headers.add(SUBTITLE_HEADER, subtitle);

	ResponseEntity<String> response = new ResponseEntity<String>(newLog.substring(startAt), headers, HttpStatus.OK);
	return response;
    }

    private static Map<String, Object> step2Model(long jobId, JobStep step) {
	Map<String, Object> row = new HashMap<>();
	row.put("jobid", jobId);
	row.put("step", step.getStep());
	row.put("executeOn", step.getExecuteOn().toString());
	row.put("playbook", Optional.ofNullable(step.getPlaybook()).orElse(step.getKind().toString()));
	row.put("status", step.getStatus());
	row.put("startedAt", Optional.ofNullable(step.getStartedAt()).map(i -> i.truncatedTo(ChronoUnit.SECONDS))
		.map(i -> DateTimeFormatter.ISO_INSTANT.format(i)).orElse("now"));
	row.put("finishedAt", Optional.ofNullable(step.getFinishedAt()).map(i -> i.truncatedTo(ChronoUnit.SECONDS))
		.map(i -> DateTimeFormatter.ISO_INSTANT.format(i)).orElse("now"));

	row.put("startedAgo", Optional.ofNullable(step.getStartedAt()).map(Date::from)
		.map(JobController.prettyFormatter::format).orElse(""));

	row.put("runningFor", "");
	if (Objects.nonNull(step.getStartedAt())) {
	    Duration running = Duration.between(step.getStartedAt(),
		    Optional.ofNullable(step.getFinishedAt()).orElseGet(Instant::now));
	    if (running.getSeconds() < 60) {
		row.put("runningFor", String.format("%d sec", running.getSeconds()));
	    } else {
		row.put("runningFor", String.format("%d min", Math.floorDiv(running.getSeconds(), 60L)));
	    }
	}

	return row;
    }

    private static String buildTitle(long jobId, Job job) {
	return String.format("Job %d: %s on %s (%s)", jobId, job.getWorkflow(), job.getEnvironment(), job.getVersion());
    }

    private static String buildStepSubtitle(Job job, JobStep jobStep) {
	return String.format("Step %s started by %s at %s, %s %s",
		Optional.ofNullable(jobStep.getPlaybook()).orElse(jobStep.getKind().toString()), job.getUser(),
		Optional.ofNullable(jobStep.getStartedAt()).map(Date::from).map(JobController.prettyFormatter::format)
			.orElse("now"),
		jobStep.getStatus().name(),
		Optional.ofNullable(jobStep.getFinishedAt()).map(i -> i.truncatedTo(ChronoUnit.SECONDS))
			.map(i -> DateTimeFormatter.ISO_INSTANT.format(i)).orElse("now"));
    }

    private static String buildJobSubtitle(Job job) {

	return String.format("Started by %s %s, %s %s", job.getUser(),
		Optional.ofNullable(job.getStartedAt()).map(Date::from).map(JobController.prettyFormatter::format)
			.orElse("now"),
		job.getStatus().name(),
		Optional.ofNullable(job.getFinishedAt()).map(i -> i.truncatedTo(ChronoUnit.SECONDS))
			.map(i -> DateTimeFormatter.ISO_INSTANT.format(i)).orElse("now"));
    }
}
