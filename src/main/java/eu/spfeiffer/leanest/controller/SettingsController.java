package eu.spfeiffer.leanest.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.boot.info.BuildProperties;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import eu.spfeiffer.leanest.config.LeanestConfig;
import eu.spfeiffer.leanest.config.LeanestConfig.EnvironmentConfig;
import eu.spfeiffer.leanest.config.LeanestConfig.VersionConfig;
import eu.spfeiffer.leanest.config.LeanestConfig.WorkflowConfig;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class SettingsController {
    private final BuildProperties buildProperties;
    private final LeanestConfig config;

    @GetMapping("/settings")
    public ModelAndView settings(CsrfToken token, Principal user, Map<String, Object> model) {
	model.put("_csrf", token);
	model.put("user", user.getName());

	List<Map.Entry<String, VersionConfig>> versions = new ArrayList<>(config.getVersions().entrySet());
	Collections.sort(versions, Comparator.comparing(Map.Entry::getKey));
	model.put("versions", versions);

	List<Map.Entry<String, EnvironmentConfig>> environments = new ArrayList<>(config.getEnvironments().entrySet());
	Collections.sort(environments, Comparator.comparing(Map.Entry::getKey));
	model.put("environments", environments);

	Map<String, WorkflowConfig> workflowmap = config.getWorkflows();
	workflowmap.replaceAll((s, w) -> {
	    w.getSteps().replaceAll(this::validateStep);
	    return w;
	});
	List<Map.Entry<String, WorkflowConfig>> workflows = new ArrayList<>(config.getWorkflows().entrySet());

	Collections.sort(workflows, Comparator.comparing(Map.Entry::getKey));
	model.put("workflows", workflows);

	model.put("workdir", config.getWorkDir());
	model.put("gitremote", config.getGitRemote());

	model.put("leanest-version", buildProperties.getVersion());
	model.put("leanest-timestamp", buildProperties.getTime().toString());

	return new ModelAndView("settings", model);
    }

    private String validateStep(String s) {
	if (s.startsWith("workflow:")) {
	    String workflowname = s.substring("workflow:".length()).trim();
	    if (!config.getWorkflows().keySet().contains(workflowname)) {
		return String.format("<span class=\"error\" title=\"Referenced workflow not found!\">%s</span>", s);
	    }
	}
	return s;
    }
}
