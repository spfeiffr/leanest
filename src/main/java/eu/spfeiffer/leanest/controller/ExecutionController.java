package eu.spfeiffer.leanest.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import eu.spfeiffer.leanest.model.ExecutionRequest;
import eu.spfeiffer.leanest.service.JobExecutionService;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class ExecutionController {

    private final JobExecutionService jobExecutionService;

    @PostMapping("/execution")
    public RedirectView startExecution(Principal user, @ModelAttribute ExecutionRequest executionRequest,
	    RedirectAttributes redirectAttributes) {
	if (executionRequest.getEnvironment().isEmpty()) {
	    redirectAttributes.addFlashAttribute("error", "No Environment selected!");
	    return new RedirectView("/");
	}

	long jobId = jobExecutionService.createJob(user.getName(), executionRequest);
	return new RedirectView(String.format("/job/%d/", jobId));
    }

    @DeleteMapping("/job/{jobId}")
    @ResponseBody
    public void abortExecution(@PathVariable long jobId) {
	jobExecutionService.requestAbortJob(jobId);
    }
}
