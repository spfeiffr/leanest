package eu.spfeiffer.leanest;

import static org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter.DEFAULT_AUTHORIZATION_REQUEST_BASE_URI;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mustache.MustacheEnvironmentCollector;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.client.RestTemplate;

import com.samskivert.mustache.Mustache;

import eu.spfeiffer.leanest.config.LeanestConfig;
import lombok.RequiredArgsConstructor;

@SpringBootApplication
@EnableConfigurationProperties
@RequiredArgsConstructor
public class LeanestApplication implements ApplicationRunner {

    private final LeanestConfig config;

    public static void main(String[] args) {
	SpringApplication.run(LeanestApplication.class, args);
    }

    @Configuration
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    class SecurityConfig {

	/**
	 * Configures OAuth Login with Spring Security 5.
	 * 
	 * @return
	 */
	@Bean
	public WebSecurityConfigurerAdapter webSecurityConfigurer(
		@Value("${keycloak-client.registration-id}") final String registrationId,
		KeycloakLogoutHandler keycloakLogoutHandler) {
	    return new WebSecurityConfigurerAdapter() {
		@Override
		public void configure(HttpSecurity http) throws Exception {
		    http
			    // Configure session management to your needs.
			    // I need this as a basis for a classic, server side
			    // rendered application
			    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
			    // Depends on your taste. You can configure single
			    // paths here
			    // or allow everything a I did and then use method
			    // based security
			    // like in the controller below
			    .authorizeRequests().anyRequest().authenticated().and().logout()
			    // propagate the Spring Security /logout function to
			    // Keycloak
			    .addLogoutHandler(keycloakLogoutHandler).and()
			    // This is the point where OAuth2 login of Spring 5
			    // gets enabled
			    .oauth2Login()
			    // I don't want a page with different clients as
			    // login options
			    // So i use the constant from
			    // OAuth2AuthorizationRequestRedirectFilter
			    // plus the configured realm as immediate redirect
			    // to Keycloak
			    .loginPage(DEFAULT_AUTHORIZATION_REQUEST_BASE_URI + "/" + registrationId);
		}
	    };
	}
    }

    @Bean
    KeycloakLogoutHandler keycloakLogoutHandler() {
	return new KeycloakLogoutHandler(new RestTemplate());
    }

    @Bean
    public Mustache.Compiler mustacheCompiler(Mustache.TemplateLoader mustacheTemplateLoader, Environment environment) {
	MustacheEnvironmentCollector collector = new MustacheEnvironmentCollector();
	collector.setEnvironment(environment);

	// default value
	Mustache.Compiler compiler = Mustache.compiler().defaultValue("").withLoader(mustacheTemplateLoader)
		.withCollector(collector);
	return compiler;
    }

    @Bean
    public TaskExecutor threadPoolTaskExecutor() {
	ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	executor.setCorePoolSize(20);
	executor.setMaxPoolSize(60);
	executor.setThreadNamePrefix("default_task_executor_thread");
	executor.initialize();

	return executor;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
	if (Objects.nonNull(config.getWorkDir())) {
	    Files.createDirectories(Paths.get(config.getWorkDir()));
	}
    }
}
