package eu.spfeiffer.leanest.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import eu.spfeiffer.leanest.model.entities.Job;

@Repository
public interface JobRepository extends PagingAndSortingRepository<Job, Long> {
}
