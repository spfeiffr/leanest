package eu.spfeiffer.leanest.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import eu.spfeiffer.leanest.model.entities.StepLog;

@Repository
public interface StepLogRepository extends CrudRepository<StepLog, Long> {
    public Optional<StepLog> findByJobAndStep(long job, int step);
}
