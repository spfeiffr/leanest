package eu.spfeiffer.leanest.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import eu.spfeiffer.leanest.config.LeanestConfig;
import eu.spfeiffer.leanest.config.LeanestConfig.WorkflowConfig;
import eu.spfeiffer.leanest.execution.JobRunner;
import eu.spfeiffer.leanest.model.ExecutionRequest;
import eu.spfeiffer.leanest.model.entities.Job;
import eu.spfeiffer.leanest.model.entities.JobStep;
import eu.spfeiffer.leanest.model.enums.ExecutionRule;
import eu.spfeiffer.leanest.model.enums.JobStatus;
import eu.spfeiffer.leanest.model.enums.StepKind;
import eu.spfeiffer.leanest.repository.JobRepository;
import eu.spfeiffer.leanest.repository.StepLogRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class JobExecutionService {
    private final TaskExecutor taskExecutor;
    private final JobRepository jobRepository;
    private final StepLogRepository logRepository;
    private final LeanestConfig config;

    private Set<Long> abortRequestedJobIds = new HashSet<>();

    private static final String WORKFLOW_PREFIX = "workflow:";

    public void updateJob(Job job) {
	jobRepository.save(job);
	if (job.getStatus() == JobStatus.ABORTED) {
	    abortRequestedJobIds.remove(job.getId());
	}
    }

    public Long createJob(String username, ExecutionRequest executionRequest) {
	Job job = new Job();
	job.setVersion(executionRequest.getVersion());
	job.setWorkflow(executionRequest.getWorkflow());
	job.setEnvironment(executionRequest.getEnvironment());
	job.setUser(username);
	job.setStatus(JobStatus.NOT_YET_STARTED);
	job.setStartedAt(Instant.now());
	job.setSteps(createStepList(executionRequest.getWorkflow()));

	jobRepository.save(job);

	taskExecutor.execute(new JobRunner(job, this, logRepository, taskExecutor, config));

	return job.getId();
    }

    public void requestAbortJob(Long jobId) {
	Optional<Job> job = jobRepository.findById(jobId);
	if (!job.isPresent()) {
	    return;
	}
	if (job.get().getStatus() == JobStatus.NOT_YET_STARTED || job.get().getStatus() == JobStatus.RUNNING) {
	    abortRequestedJobIds.add(job.get().getId());
	}
    }

    public boolean isJobAbortionRequested(Job job) {
	return abortRequestedJobIds.contains(job.getId());
    }

    private List<JobStep> createStepList(String workflow) {
	List<JobStep> steps = new ArrayList<>();

	JobStep execution = new JobStep();
	execution.setStatus(JobStatus.NOT_YET_STARTED);
	execution.setKind(StepKind.GIT_CHECKOUT);
	execution.setPlaybook(StepKind.GIT_CHECKOUT.toString());
	execution.setExecuteOn(ExecutionRule.ALWAYS);
	execution.setStep(0);
	steps.add(execution);

	if (!steps.addAll(resolveSteps(workflow, 0, Collections.<String>emptySet()))) {
	    return new ArrayList<>();
	}

	execution = new JobStep();
	execution.setStatus(JobStatus.NOT_YET_STARTED);
	execution.setKind(StepKind.CLEANUP);
	execution.setPlaybook(StepKind.CLEANUP.toString());
	execution.setExecuteOn(ExecutionRule.ALWAYS);
	execution.setStep(steps.size() + 1);
	steps.add(execution);

	return steps;
    }

    private List<JobStep> resolveSteps(String workflow, int currentStep, Set<String> visitedFlows) {
	WorkflowConfig workflowConfig = config.getWorkflows().get(workflow);
	if (Objects.isNull(workflowConfig)) {
	    return new ArrayList<>();
	}

	JobStep execution;
	List<JobStep> steps = new ArrayList<>();

	for (String step : workflowConfig.getSteps()) {
	    if (step.startsWith(WORKFLOW_PREFIX)) {
		step = step.substring(WORKFLOW_PREFIX.length()).trim();
		if (visitedFlows.contains(step)) {
		    // break loops in workflow definition graph
		    return new ArrayList<>();
		}
		Set<String> visitedFlowsCopy = new HashSet<>(visitedFlows);
		visitedFlowsCopy.add(step);
		List<JobStep> referencedSteps = resolveSteps(step, currentStep, visitedFlowsCopy);
		currentStep += referencedSteps.size();
		steps.addAll(referencedSteps);
	    } else {
		currentStep = currentStep + 1;
		execution = new JobStep();
		execution.setPlaybook(step);
		execution.setStatus(JobStatus.NOT_YET_STARTED);
		execution.setKind(StepKind.CMD);
		execution.setExecuteOn(ExecutionRule.ON_SUCCESS);
		execution.setStep(currentStep);
		steps.add(execution);
	    }
	}

	return steps;
    }
}
