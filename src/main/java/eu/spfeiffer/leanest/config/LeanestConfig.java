package eu.spfeiffer.leanest.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties("leanest")
@Data
@RefreshScope
public class LeanestConfig {

    private String gitRemote = null;

    private String workDir = null;

    private Map<String, VersionConfig> versions = new HashMap<>();

    private Map<String, EnvironmentConfig> environments = new HashMap<>();

    private Map<String, WorkflowConfig> workflows = new HashMap<>();

    @Data
    public static class VersionConfig {
	private String branch = null;
    }

    @Data
    public static class EnvironmentConfig {
	private String sudoPassword = null;
	private String inventory = null;
    }

    @Data
    public static class WorkflowConfig {
	private List<String> steps = new ArrayList<>();
    }
}
