create table "job" (
	"id"  bigserial not null, 
	"environment" varchar(255), 
	"finished_at" timestamp, 
	"started_at" timestamp, 
	"status" varchar(255), 
	"user" varchar(255), 
	"version" varchar(255), 
	"workflow" varchar(255), 
	primary key ("id"));
	
create table "job_steps" (
	"job_id" int8 not null, 
	"execute_on" varchar(255), 
	"exit_code" int4 not null, 
	"finished_at" timestamp, 
	"kind" varchar(255), 
	"playbook" varchar(1024), 
	"started_at" timestamp, 
	"status" varchar(255), 
	"step" int4);
	
create table "step_log" (
	"id"  bigserial not null, 
	"job" int8 not null, 
	"linecount" int8 not null, 
	"log" text, 
	"step" int4 not null, 
	primary key ("id"));

alter table "job_steps" add constraint "job_steps_job" foreign key ("job_id") references "job";
