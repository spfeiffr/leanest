package eu.spfeiffer.leanest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = { "spring.flyway.enabled=false" })
public class LeanestApplicationTests {

    @Test
    public void contextLoads() {
    }

}
