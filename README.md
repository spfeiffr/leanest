# Leanest

Executing Git versioned Ansible playbooks via a web UI

## Required additional components

- Ansible installed on the same machine (in the path)
- Git installed on the same machine (in the path)
- PostgreSQL 9.x
- Keycloak

## Default configuration `classpath:/application.yml`

- `server.port: 1409`
- `keycloak-client.realm: leanestRealm`
- `keycloak-client.registration-id: leanest`
- `keycloak-client.server-url`
- `spring.datasource.url: jdbc:postgresql://localhost:5432/leanest`
- `spring.datasource.username: leanest`
- `spring.datasource.password: leanest`

To overwrite these values and set the `keycloak-client.server-url` the config location should be extended by another file:
```
java -Dspring.config.location=classpath:/application.yml,settings.yml -jar leanest-0.0.1-SNAPSHOT.jar
```
This allows to provide additional values in the `settings.yml` file sitting beside the jar file. This also would be the place to define branches, environments and workflows.

## Keycloak configuration
- `Realm: leanestRealm`
- `Client: leanest-client, openid-connect, valid Redirect URLs and Base URL = URL where Leanest is running`
- `Roles: user`
Users need to have the role `user` assigned to access Leanest.

## Running Leanest as a systemd service
```
[Unit]
Description=Leanest
Wants=network-online.target
After=network-online.target

[Service]
WorkingDirectory=<…>

User=<…>
Group=<…>

ExecStart=java -Dspring.config.location=classpath:/application.yml,settings.yml -jar leanest-0.0.1-SNAPSHOT.jar

StandardOutput=journal
StandardError=inherit

[Install]
WantedBy=multi-user.target
```

## Running Leanest and Keycloak behind NGINX on a single port

Leanest running on port 1409, Keycloak running on port 8080, this NGINX config snippet both offers them at port 80:
```
    server {
        listen 80;

        location /auth/ {
            proxy_pass          http://localhost:8080/auth/;
            proxy_set_header    Host               $host;
            proxy_set_header    X-Real-IP          $remote_addr;
            proxy_set_header    X-Forwarded-For    $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Host   $host;
            proxy_set_header    X-Forwarded-Server $host;
            proxy_set_header    X-Forwarded-Port   $server_port;
            proxy_set_header    X-Forwarded-Proto  $scheme;
        }

        location / {
            proxy_pass          http://localhost:1409/;
            proxy_set_header    Host               $host;
            proxy_set_header    X-Real-IP          $remote_addr;
            proxy_set_header    X-Forwarded-For    $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Host   $host;
            proxy_set_header    X-Forwarded-Server $host;
            proxy_set_header    X-Forwarded-Port   $server_port;
            proxy_set_header    X-Forwarded-Proto  $scheme;
        }
    }
```